//1. using para trabajar con EntityFramework 
using Microsoft.EntityFrameworkCore;
using UniversityApliBackend.DataAccess;

var builder = WebApplication.CreateBuilder(args);

//2. Conectar con la base de datos SQL
const string CONNECTIONNAME = "UniversityDB";
var connectionString= builder.Configuration.GetConnectionString(CONNECTIONNAME);

//3. Agregar contexto de servicios al builder
builder.Services.AddDbContext<UniversityDBContext>(option=>option.UseSqlServer(connectionString));




// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
