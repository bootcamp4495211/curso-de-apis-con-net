﻿using System.ComponentModel.DataAnnotations;

namespace UniversityApliBackend.Models.DataModels
{
    public class Student: BaseEntity
    {
        [Required]
        public string Firstname { get; set; } = string.Empty;

        [Required]
        public string Lastname { get; set; }= string.Empty;

        [Required]
        public DateTime DoB { get; set; }

        public ICollection<Course> Courses { get; set; }= new List<Course>();


    }
}
