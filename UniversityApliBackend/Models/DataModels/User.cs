﻿using System.ComponentModel.DataAnnotations;

namespace UniversityApliBackend.Models.DataModels
{
    public class User
    {

        [Required]
        [Key]
        public int Id { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; } = string.Empty;

        [Required, StringLength(100)]
        public string Lastname { get; set; } = string.Empty;

        [Required, EmailAddress]
        public string Email { get; set; } = string.Empty;

        [Required]
        public string Password { get; set; } = string.Empty;


        public string CreatedBy { get; set; } = string.Empty;
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public string UpdateBy { get; set; } = string.Empty;
        public DateTime? UpdateAt { get; set; }

        public string DeleteBy { get; set; } = string.Empty;

        public DateTime? DeleteAt { get; set; }
        public bool IsDeleted { get; set; } = false;

    }
}
