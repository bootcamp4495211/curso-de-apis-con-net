﻿using System.ComponentModel.DataAnnotations;

namespace UniversityApliBackend.Models.DataModels
{
    public class BaseEntity
    {
        private const bool bDelete = false;

        [Required]
        [Key]
        public int Id { get; set; }
        public User CreatedBy { get; set; } = new User();
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public User UpdateBy { get; set; } = new User();
        public DateTime? UpdateAt { get; set; }
        public User DeleteBy { get; set; } = new User();
        public DateTime? DeleteAt { get; set; }
        public bool IsDeleted { get; set; } = bDelete;
    }
}
